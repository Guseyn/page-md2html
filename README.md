# page-md2html
Provides async object `HtmlFromMd` that represents translated `html` text from `markdown` text.

[![NPM Version][npm-image]][npm-url]

## Concept

This library is based on this [one](https://github.com/showdownjs/showdown). So it supports regular markdown features.

## Usage

```js

new HtmlFromMd(markdownText).call();

```

[npm-image]: https://img.shields.io/npm/v/@page-libs/md2html.svg
[npm-url]: https://npmjs.org/package/@page-libs/md2html
